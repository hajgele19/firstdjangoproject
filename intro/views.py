from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
def hello(request):
    return HttpResponse("<h2>Hello World</h2>")


@login_required()
def cars(request):
    json_cars={
        'all_cars':[
            {
                'brand': 'Dacia',
                'color': 'blue',
                'model': 'Spring',
                'year': 2022
            },
            {
                'brand': 'Audi',
                'color': 'red',
                'model': 'A4',
                'year': 2023
            },
            {
                'brand': 'Toyota',
                'color': 'black',
                'model': 'CHR',
                'year': 2021
            }
        ]
    }

    return render(request, 'intro/list_cars.html', json_cars)

@login_required()
def carti(request):
    json_carti={
        "all_carti":[
            {
                'titlu': 'Maitrey',
                'autor': 'Mircea Eliade',
                'an': 1914,
                'editura': 'Practico'
            },

            {
                'titlu': 'America',
                'autor': 'Kafka',
                'an': 1984,
                'editura': 'londsdale'

            },

            {
                'titlu': 'Pestera',
                'autor': 'Platon',
                'an': 1250,
                'editura': 'Socrates'
            },

            {
                'titlu': 'Spiderweb',
                'autor': 'Phlilip Shelby',
                'an': 1969,
                'editura': 'I.P.C'

            }
        ]

    }

    return render(request, 'intro/list_carti.html', json_carti)