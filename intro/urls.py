from django.urls import path

from intro import views

urlpatterns = [
    path('tesxt/', views.hello, name='hello_message'),
    path('list_of_cars/', views.cars, name='list-of-cars'),
    path('list_of_carti/', views.carti, name='list-of-carti'),
]
