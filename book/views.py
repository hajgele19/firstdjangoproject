from pprint import pprint

from django.shortcuts import render, redirect
import bs4
import requests
from django.utils.html import escape

from book.models import Book


def get_books_emag(request):
	url = 'https://www.emag.ro/search/laptop?ref=effective_search'
	result = requests.get(url)
	soup = bs4.BeautifulSoup(result.text, 'lxml' )
	content = {'data': []}
	cases = soup.find_all('div', class_='card-v2')

	for case in cases:
		details_book = {}
		book_title = case.find('a', class_="card-v2-title semibold mrg-btm-xxs js-product-url")
		book_price = case.find('p', class_='product-new-price')

		if book_title is None:
			details_book['title'] = 'No data'
		else:
			details_book['title'] = escape(book_title.text)

		if book_price is None:
			details_book['price'] = 'No data'
		else:
			details_book['price'] = escape(book_price.text)

		content['data'].append(details_book)

	for product in content['data']:
		Book.objects.create(title=product['title'], price=product['price'])


	return redirect('home_page')