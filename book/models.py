from django.db import models

# Create your models here.
class Book(models.Model):
    title = models.TextField(max_length=400, null=True)
    price = models.CharField(max_length=30, null=True)

    def __str__(self):
        return self.title
