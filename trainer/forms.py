from django import forms
from django.forms import TextInput, NumberInput, EmailInput, Textarea, DateTimeInput, DateInput, Select


from trainer.models import Trainer


class TrainerForm(forms.ModelForm):
    class Meta:
        model = Trainer
        # fields = '__all__'
        fields = ['first_name', 'last_name', 'course', 'email', 'department']


        widgets ={
            'first_name': TextInput(attrs={'placeholder': 'Please enter your first name', 'class': 'form-control'}),
            'last_name': TextInput(attrs={'placeholder': 'Please enter your last name', 'class': 'form-control'}),
            'course': TextInput(attrs={'placeholder': 'Please enter your course name', 'class': 'form-control'}),
            'email': EmailInput(attrs={'placeholder': 'Please enter your email', 'class': 'form-control'}) ,
            'department': Select(attrs={'class': 'form-select'}),


        }




class TrainerUpdateForm(forms.ModelForm):
    class Meta:
        model = Trainer
        # fields = '__all__'
        fields = ['first_name', 'last_name', 'course', 'email', 'department']


        widgets ={
            'first_name': TextInput(attrs={'placeholder': 'Please enter your first name', 'class': 'form-control'}),
            'last_name': TextInput(attrs={'placeholder': 'Please enter your last name', 'class': 'form-control'}),
            'course': TextInput(attrs={'placeholder': 'Please enter your course name', 'class': 'form-control'}),
            'email': EmailInput(attrs={'placeholder': 'Please enter your email', 'class': 'form-control'}) ,
            'department': Select(attrs={'class': 'form-select'}),


        }