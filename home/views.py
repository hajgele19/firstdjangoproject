from django.shortcuts import render
from django.views.generic import TemplateView


# templateview->este un view generic pentru a randa/afisa o pagina html-> in interiorul clasei Hometemplateview folosind
# templateview veti mai multe metode specifice pentru acest view generic
class HomeTemplateView(TemplateView):
    template_name = 'home/homepage.html' #specificam calea catre pagina html(paginile html se afla in folderul TEMPLATES)

