from datetime import datetime
from random import randint

from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.core.mail import send_mail, EmailMessage
from django.shortcuts import render, redirect
from django.template.loader import get_template
from django.urls import reverse_lazy
from django.views.generic import CreateView

from djangoProject.settings import EMAIL_HOST_USER
from userextend.forms import UserNewForm
from userextend.models import UserHistory


class UserCreateView(CreateView):
    template_name = 'userextend/create_user.html'
    model = User
    form_class = UserNewForm
    success_url = reverse_lazy('home_page')

    def form_valid(self, form):
        if form.is_valid():
            new_user = form.save(commit=False)
            new_user.first_name = new_user.first_name.title()
            new_user.last_name = new_user.last_name.upper()
            new_user.username = f"{new_user.first_name[0].lower()}{new_user.last_name.replace(' ', '').lower()}_{str(randint(100000, 999999))}"
            print(f"username = {new_user.username}")
            new_user.save()



            # Trimitere mail fara template
            # subject = 'Cont nou'
            # message = f'Felicitari {new_user.first_name} {new_user.last_name}.' \
            #           f'Ti-ai creat cont in aplicatie. Username-ul tau este {new_user.username}'
            # send_mail(subject, message, EMAIL_HOST_USER, [new_user.email])
            # return redirect('login')




            # Trimitere mail CU template

            details_user = {
                'full_name': f'{new_user.first_name} {new_user.last_name}',
                'username': f'{new_user.username}'
            }

            subject = 'Contul tau a fost creat in aplicatie!'
            message = get_template('mail.html').render(details_user)
            mail = EmailMessage(subject, message, EMAIL_HOST_USER, [new_user.email])
            mail.content_subtype = 'html'
            mail.send()

            new_message = f'La data de {datetime.now()} a fost adaugat userul cu urmatoarele informatii:' \
                          f'username:{new_user.username}, first_name: {new_user.first_name}, ' \
                          f'last_name: {new_user.last_name}, email:{new_user.email}'
            UserHistory.objects.create(message=new_message, created_at=datetime.now(), updated_at=datetime.now())

            return redirect('login')