from datetime import datetime

from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView, UpdateView, DeleteView, DetailView

from student.filters import StudentFilter
from student.forms import StudentForm, StudentUpdateForm
from student.models import Student
from trainer.models import Trainer


#CreateView -> il folosim pt a generea un formulat pe baza modelui studenti din models.py si a salva datele in tabela student_student

# LoginRequiredMixin este o clasa definita in Django care este utilizata pentru a restrictiona
# accesul la clasa daca utilziatorul NU este autentificat.
class StudentCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    template_name = 'student/create_student.html'
    model = Student
    # fields = '__all__'
    # fields = ['first_name', 'last_name', 'age', 'email', 'description', 'start_date', 'end_date', 'active', 'gender', 'trainer']
    form_class = StudentForm
    permission_required = 'student.add_student'
    success_url = reverse_lazy('home_page')

# ListView -> a ranada/ a afisa date dintro tabela.
class StudentListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    template_name = 'student/list_of_students.html'
    model = Student
    permission_required = 'student.view_list_of_students'
    context_object_name = 'all_students' #Students.objiects.all()

    def get_queryset(self):  # returnati un query cu datele pe care le doriti in interfata.
        return Student.objects.filter(active=True)

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        now = datetime.now()
        data['current_datetime'] = now
        trainer = Trainer.objects.all()
        data['list_trainers'] = trainer
        print(data)

        students = Student.objects.filter(active=True)# realizam un query prin care ii luam pe toti active(studenti)
        my_filter = StudentFilter(self.request.GET, queryset=students) # in momentul in care realizam o filtrare ii vom cauta prin cei care au active=True(studenti)
        students = my_filter.qs
        data['all_students'] = students
        data['form_filters'] = my_filter.form

        return data

class StudentUpdateView(LoginRequiredMixin, UpdateView):
    template_name = 'student/update_student.html'
    model = Student
    form_class = StudentUpdateForm
    success_url = reverse_lazy('list-of-students')



class StudentDeleteView(LoginRequiredMixin, DeleteView):
    template_name = 'student/delete_student.html'
    model = Student
    success_url = reverse_lazy('list-of-students')


class StudentDetailView(LoginRequiredMixin, DetailView):
    template_name = 'student/details_student.html'
    model = Student

