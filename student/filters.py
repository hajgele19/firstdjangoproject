import django_filters

import django_filters
from django import forms
from django_filters import DateFilter, DateTimeFilter

from student.models import Student

# Definim formularul de filtrare in care specificam campurile de filtrare pe care le dorim in formular in modelul Student
class StudentFilter(django_filters.FilterSet):
    list_of_students = [(student.first_name, student.first_name.upper()) for student in
                        Student.objects.filter(active=True)]
    first_name = django_filters.ChoiceFilter(choices=list(set(list_of_students)))

    # first_name = django_filters.CharFilter(lookup_expr='icontains', label='First name')

    # first_name = django_filters.CharFilter(lookup_expr='icontains', label='First name')
    last_name = django_filters.CharFilter(lookup_expr='icontains', label='First name')

    start_date_gte = DateTimeFilter(field_name='start_date', lookup_expr='gte', label = 'From start date',
                                widget= forms.DateTimeInput(attrs={'class': 'form-control', 'type': 'datetime-local'}))
    start_date_lte = DateTimeFilter(field_name='start_date', lookup_expr='lte', label = 'To start date',
                                widget= forms.DateTimeInput(attrs={'class': 'form-control', 'type': 'datetime-local'}))

    class Meta:
        model = Student
        fields = ['first_name', 'last_name', 'age', 'start_date_gte', 'start_date_lte', 'trainer']


    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.filters['first_name'].field.widget.attrs.update({'class': 'form-control', 'placeholder': 'Please enter first name'})
        self.filters['last_name'].field.widget.attrs.update({'class': 'form-control', 'placeholder': 'Please enter last name'})
        self.filters['age'].field.widget.attrs.update({'class': 'form-control', 'placeholder': 'Please enter age'})
        self.filters['trainer'].field.widget.attrs.update({'class': 'form-select'})