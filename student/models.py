from django.db import models

from trainer.models import Trainer


class Student(models.Model):

    gender_options = (('male', 'Male'), ('female', 'Female'))

    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    age = models.IntegerField()
    email = models.EmailField(max_length=100)
    description = models.TextField(max_length=300)
    start_date = models.DateTimeField()
    end_date = models.DateField()
    active = models.BooleanField(default=True)
    gender = models.CharField(max_length=6, choices=gender_options)

    trainer = models.ForeignKey(Trainer, on_delete=models.CASCADE, null=True)

    created_at = models.DateTimeField(auto_now_add=True, null=True) #auto_now_add=True il folosim pentru a stoca data si ora cand a fost creata inregistrarea
    updated_at = models.DateTimeField(auto_now=True, null=True) # auto_now il folosim pt a stoca data si ora de fiecare data can actualizam informatii despre studentul nostru


    def __str__(self):
        return f'{self.first_name} {self.last_name}'