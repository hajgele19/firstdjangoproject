from django import forms
from django.forms import TextInput, NumberInput, EmailInput, Textarea, DateTimeInput, DateInput, Select

from student.models import Student




class StudentForm(forms.ModelForm):
    class Meta:
        model = Student
        # fields = '__all__'
        fields = ['first_name', 'last_name', 'age',
                  'email', 'description', 'start_date',
                  'end_date', 'active', 'gender', 'trainer']


        widgets ={
            'first_name': TextInput(attrs={'placeholder': 'Please enter your first name', 'class': 'form-control'}),
            'last_name': TextInput(attrs={'placeholder': 'Please enter your last name', 'class': 'form-control'}),
            'age': NumberInput(attrs={'placeholder': 'Please enter your age', 'class': 'form-control'}) ,
            'email': EmailInput(attrs={'placeholder': 'Please enter your email', 'class': 'form-control'}) ,
            'description': Textarea(attrs={'placeholder': 'Please enter your description', 'class': 'form-control'}) ,
            'start_date': DateTimeInput(attrs={'class': 'form-control', 'type': 'datetime-local'}),
            'end_date': DateInput(attrs={'class': 'form-control', 'type': 'date'}),
            'gender': Select(attrs={'class': 'form-select'}),
            'trainer': Select(attrs={'class': 'form-select'}),

        }

    def clean(self):
        cleaned_data = self.cleaned_data  # stocam in variabila cleaned_data un dictionar cu valorile completate de utilizator in formular
        all_students = Student.objects.filter(email=cleaned_data['email'])
        if all_students:
            msg = 'Aceasta adresa de email exista deja.'
            self._errors['email'] = self.error_class([msg])


        # return cleaned_data


        if cleaned_data.get('start_date').date() > cleaned_data.get('end_date'):
            msg = 'Start Date nu poate fi mai mare decat End Date'
            self._errors['end_date'] = self.error_class([msg])


        return cleaned_data



class StudentUpdateForm(forms.ModelForm):
    class Meta:
        model = Student
        fields = ['first_name', 'last_name', 'age',
                  'email', 'description', 'start_date',
                  'end_date', 'active', 'gender', 'trainer']


        widgets ={
            'first_name': TextInput(attrs={'placeholder': 'Please enter your first name', 'class': 'form-control'}),
            'last_name': TextInput(attrs={'placeholder': 'Please enter your last name', 'class': 'form-control'}),
            'age': NumberInput(attrs={'placeholder': 'Please enter your age', 'class': 'form-control'}) ,
            'email': EmailInput(attrs={'placeholder': 'Please enter your email', 'class': 'form-control'}) ,
            'description': Textarea(attrs={'placeholder': 'Please enter your description', 'class': 'form-control'}) ,
            'start_date': DateTimeInput(attrs={'class': 'form-control', 'type': 'datetime-local'}),
            'end_date': DateInput(attrs={'class': 'form-control', 'type': 'date'}),
            'gender': Select(attrs={'class': 'form-select'}),
            'trainer': Select(attrs={'class': 'form-select'}),

        }
